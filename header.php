<?php $options = get_desing_plus_option(); ?>
<!DOCTYPE html>
<html class="pc" <?php language_attributes(); ?>>
<?php
     $options = get_desing_plus_option();
     if($options['use_ogp']) {
?>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<?php } else { ?>
<head>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<?php }; ?>
<meta charset="<?php bloginfo('charset'); ?>">
<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
<?php if ( is_no_responsive() && wp_is_mobile() ) : ?>
<meta name="viewport" content="width=1280">
<?php else : ?>
<meta name="viewport" content="width=device-width">
<?php endif; ?>
<title><?php wp_title('|', true, 'right'); ?></title>
<meta name="description" content="<?php seo_description(); ?>">
<?php if($options['use_ogp']) { ogp(); }; ?>
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
<?php
     if ( $options['favicon'] ) {
       $favicon_image = wp_get_attachment_image_src( $options['favicon'], 'full');
       if(!empty($favicon_image)) {
?>
<link rel="shortcut icon" href="<?php echo esc_url($favicon_image[0]); ?>">
<?php }; }; ?>
<?php wp_enqueue_style('style', get_stylesheet_uri(), false, version_num(), 'all'); wp_enqueue_script( 'jquery' ); if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
<?php wp_head(); ?>
</head>
<body id="body" <?php body_class(); ?>>

<div id="site_loader_overlay">
 <div id="site_loader_spinner"></div>
</div>
<div id="site_wrap">

 <div id="header">
  <div id="header_inner" class="clearfix">
   <div class="title-wrap">
     <img src="http://kurashi-develop.sakura.ne.jp/newkurashitech/wp-content/uploads/2019/06/kurashitech-pc-logo.png" alt="くらしテック" class="pc">
     <img src="http://kurashi-develop.sakura.ne.jp/newkurashitech/wp-content/uploads/2019/06/kurashitech-logo-sp.png" alt="くらしテック" class="sp">
   </div>
   <?php if (has_nav_menu('global-menu')) { ?>
   <div id="global_menu">
    <?php wp_nav_menu( array( 'sort_column' => 'menu_order', 'theme_location' => 'global-menu' , 'container' => '' ) ); ?>
   </div>
   <a href="#" class="menu_button"><span><?php _e('menu', 'tcd-w'); ?></span></a>
   <?php }; ?>
  </div>
 </div><!-- END #header -->


 <div id="top">

  <?php
       // header slider -------------------------------------------------------
       if(is_front_page()) {
         // header slider -------------------------------------------------------------------------------------------------
         if($options['header_content_type'] == 'type1') {
  ?>
  <div id="header_slider"<?php if( $options['use_slider_nav'] != 1) { echo ' class="no_slider_nav"'; }; ?>>
   <?php
        for($i=1; $i<= 3; $i++):
	        if (is_mobile()) {
            if(!empty($options['slider_image_mobile'.$i])):
              $image = wp_get_attachment_image_src( $options['slider_image_mobile'.$i], 'full');
            else:
              if(!empty($options['slider_image'.$i])){
                $image = wp_get_attachment_image_src( $options['slider_image'.$i], 'full');
              }else{
                $image = null;
              }
            endif;
          } else {
            $image = wp_get_attachment_image_src( $options['slider_image'.$i], 'full');
          }
          if (empty($image) && $i==2) break;
          if(!empty($image)) {
            $count[]=$i;
            $url = $options['slider_url'.$i];
            $target = $options['slider_target'.$i];
            $use_catch = $options['use_slider_catch'.$i];
   ?>
   <div class="item item<?php echo $i; ?>" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;">
    <?php
         if($use_catch == 1) {
           $catch = $options['slider_catch'.$i];
           $font_size = $options['slider_catch_font_size'.$i];
           $font_color = $options['slider_catch_color'.$i];
           $shadow1 = $options['slider_catch_shadow_a'.$i];
           $shadow2 = $options['slider_catch_shadow_b'.$i];
           $shadow3 = $options['slider_catch_shadow_c'.$i];
           $shadow4 = $options['slider_catch_shadow_color'.$i];
    ?>
    <div class="caption">
     <p class="title" style="font-size:<?php echo esc_html($font_size); ?>px; text-shadow:<?php echo esc_html($shadow1); ?>px <?php echo esc_html($shadow2); ?>px <?php echo esc_html($shadow3); ?>px #<?php echo esc_html($shadow4); ?>; color:#<?php echo esc_html($font_color); ?>;"><?php echo nl2br(esc_html($catch)); ?></p>
    </div><!-- END .caption -->
    <?php }; // END if use catch ?>
    <img class="image" src="<?php echo esc_attr($image[0]); ?>" alt="" />
    <?php if($url) { ?>
    <a class="overlay" href="<?php echo esc_url($url); ?>"<?php if($target == 1) { echo ' target="_blank"'; }; ?>></a>
    <?php } else { ?>
    <div class="overlay"></div>
    <?php }; ?>
   </div><!-- END .item -->
   <?php
          } // END if has image
        endfor;
   ?>
  </div><!-- END #header_slider -->
  <?php
       // slider navigation -----------------------------------------------------------------
       if( $options['use_slider_nav'] == 1) {
  ?>
  <div id="header_slider_nav">
   <?php
        for($i=1; $i<= 3; $i++):
          $label = $options['slider_nav_label'.$i];
          $color = $options['slider_nav_color'.$i];
          $catch = $options['slider_nav_catch'.$i];
          if (!empty($count)):
            foreach($count as $val):
              if ($i===$val):
   ?>
   <div class="item clearfix">
    <?php if(!empty($label)) { ?><p class="label" style="background:#<?php echo esc_html($color); ?>;"><span><?php echo nl2br(esc_html($label)); ?></span></p><?php }; ?>
    <?php if(!empty($catch)) { ?><p class="catch"><?php echo esc_html($catch); ?></p><?php }; ?>
   </div>
   <?php endif; endforeach; endif; endfor; ?>
  </div><!-- END #header_slider_nav -->
  <?php }; // end slider navigation ?>
  <?php
       // Video ------------------------------------------------------------------------------------------------------
       } elseif($options['header_content_type'] == 'type2') {
         if(!wp_is_mobile()) { // if is pc
           $video = $options['video'];
           if(!empty($video)) {
  ?>
  <div id="header_video">
   <?php
        if($options['use_video_catch'] == 1 || $options['show_video_catch_button'] == 1) {
          $catch = esc_html($options['video_catch']);
          $font_size = $options['video_catch_font_size'];
          $desc = esc_html($options['video_desc']);
          $desc_font_size = $options['video_desc_font_size'];
          $font_color = $options['video_catch_color'];
          $shadow1 = $options['video_catch_shadow1'];
          $shadow2 = $options['video_catch_shadow2'];
          $shadow3 = $options['video_catch_shadow3'];
          $shadow4 = $options['video_catch_shadow_color'];
   ?>
   <div class="caption">
    <p class="title" style="font-size:<?php echo esc_html($font_size); ?>px; text-shadow:<?php echo esc_html($shadow1); ?>px <?php echo esc_html($shadow2); ?>px <?php echo esc_html($shadow3); ?>px #<?php echo esc_html($shadow4); ?>; color:#<?php echo esc_html($font_color); ?>;"><?php echo nl2br($catch); ?></p>
    <p class="title desc" style="font-size:<?php echo esc_html($desc_font_size); ?>px; text-shadow:<?php echo esc_html($shadow1); ?>px <?php echo esc_html($shadow2); ?>px <?php echo esc_html($shadow3); ?>px #<?php echo esc_html($shadow4); ?>; color:#<?php echo esc_html($font_color); ?>;"><?php echo nl2br($desc); ?></p>
    <?php
         if($options['show_video_catch_button'] == 1) {
           $url = $options['video_button_url'];
           $target = $options['video_button_target'];
           $button_text = $options['video_catch_button'];
    ?>
    <a class="button" href="<?php echo esc_url($url); ?>"<?php if($target == 1) { echo ' target="_blank"'; }; ?>><?php echo esc_html($button_text); ?></a>
    <?php }; // END button ?>
   </div><!-- END .caption -->
   <?php }; // END catch ?>
   <div class="overlay"></div>
  </div><!-- END #header_video -->
  <?php
           }; // END if has video
         } else { // if is mobile device ----------------------------------------------
           $image = wp_get_attachment_image_src( $options['video_image'], 'full');
           if(!empty($image)) {
  ?>
  <div id="header_slider" class="no_slider_nav">
   <div class="item item1" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;">
    <?php
         if($options['use_video_catch'] == 1 || $options['show_video_catch_button'] == 1) {
           $catch = esc_html($options['video_catch']);
           $font_size = $options['video_catch_font_size'];
           $desc = esc_html($options['video_desc']);
           $desc_font_size = $options['video_desc_font_size'];
           $font_color = $options['video_catch_color'];
           $shadow1 = $options['video_catch_shadow1'];
           $shadow2 = $options['video_catch_shadow2'];
           $shadow3 = $options['video_catch_shadow3'];
           $shadow4 = $options['video_catch_shadow_color'];
    ?>
    <div class="caption">
     <p class="title" style="font-size:<?php echo esc_html($font_size); ?>px; text-shadow:<?php echo esc_html($shadow1); ?>px <?php echo esc_html($shadow2); ?>px <?php echo esc_html($shadow3); ?>px #<?php echo esc_html($shadow4); ?>; color:#<?php echo esc_html($font_color); ?>;"><?php echo nl2br($catch); ?></p>
     <p class="title desc" style="font-size:<?php echo esc_html($desc_font_size); ?>px; text-shadow:<?php echo esc_html($shadow1); ?>px <?php echo esc_html($shadow2); ?>px <?php echo esc_html($shadow3); ?>px #<?php echo esc_html($shadow4); ?>; color:#<?php echo esc_html($font_color); ?>;"><?php echo nl2br($desc); ?></p>
     <?php
          if($options['show_video_catch_button'] == 1) {
            $url = $options['video_button_url'];
            $target = $options['video_button_target'];
            $button_text = $options['video_catch_button'];
     ?>
     <a class="button" href="<?php echo esc_url($url); ?>"<?php if($target == 1) { echo ' target="_blank"'; }; ?>><?php echo esc_html($button_text); ?></a>
     <?php }; // END button ?>
    </div><!-- END .caption -->
    <?php }; // END catch ?>
    <img class="image" src="<?php echo esc_attr($image[0]); ?>" alt="" />
    <div class="overlay"></div>
   </div>
  </div><!-- END #header_slider -->
  <?php
           }; // END if has image
         }; // END mobile device

       // Youtube -------------------------------------------------------------------------------------------------------------
       } elseif($options['header_content_type'] == 'type3') {
         if(!wp_is_mobile()) { // if is pc
           $youtube_url = $options['youtube_url'];
           if(!empty($youtube_url)) {
  ?>
  <div id="header_youtube">
   <?php
        if($options['use_video_catch'] == 1 || $options['show_video_catch_button'] == 1) {
          $catch = esc_html($options['video_catch']);
          $font_size = $options['video_catch_font_size'];
          $desc = esc_html($options['video_desc']);
          $desc_font_size = $options['video_desc_font_size'];
          $font_color = $options['video_catch_color'];
          $shadow1 = $options['video_catch_shadow1'];
          $shadow2 = $options['video_catch_shadow2'];
          $shadow3 = $options['video_catch_shadow3'];
          $shadow4 = $options['video_catch_shadow_color'];
   ?>
   <div class="caption">
    <p class="title" style="font-size:<?php echo esc_html($font_size); ?>px; text-shadow:<?php echo esc_html($shadow1); ?>px <?php echo esc_html($shadow2); ?>px <?php echo esc_html($shadow3); ?>px #<?php echo esc_html($shadow4); ?>; color:#<?php echo esc_html($font_color); ?>;"><?php echo nl2br($catch); ?></p>
    <p class="title desc" style="font-size:<?php echo esc_html($desc_font_size); ?>px; text-shadow:<?php echo esc_html($shadow1); ?>px <?php echo esc_html($shadow2); ?>px <?php echo esc_html($shadow3); ?>px #<?php echo esc_html($shadow4); ?>; color:#<?php echo esc_html($font_color); ?>;"><?php echo nl2br($desc); ?></p>
    <?php
         if($options['show_video_catch_button'] == 1) {
           $url = $options['video_button_url'];
           $target = $options['video_button_target'];
           $button_text = $options['video_catch_button'];
    ?>
    <a class="button" href="<?php echo esc_url($url); ?>"<?php if($target == 1) { echo ' target="_blank"'; }; ?>><?php echo esc_html($button_text); ?></a>
    <?php }; // END button ?>
   </div><!-- END .caption -->
   <?php }; // END catch ?>
   <div class="overlay"></div>
  </div>
  <div id="youtube_video_player" class="player" data-property="{videoURL:'<?php echo esc_url($youtube_url); ?>',containment:'#header_youtube',startAt:0,mute:true,autoPlay:true,loop:true,opacity:1}"></div>
  <?php
           }; // END if youtube url
         } else { // if is mobile device -----------------------------------------------------
           $image = wp_get_attachment_image_src( $options['youtube_image'], 'full');
           if(!empty($image)) {
  ?>
  <div id="header_slider" class="no_slider_nav">
   <div class="item item1"  style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;">
    <?php
         if($options['use_video_catch'] == 1 || $options['show_video_catch_button'] == 1) {
           $catch = esc_html($options['video_catch']);
           $font_size = $options['video_catch_font_size'];
           $desc = esc_html($options['video_desc']);
           $desc_font_size = $options['video_desc_font_size'];
           $font_color = $options['video_catch_color'];
           $shadow1 = $options['video_catch_shadow1'];
           $shadow2 = $options['video_catch_shadow2'];
           $shadow3 = $options['video_catch_shadow3'];
           $shadow4 = $options['video_catch_shadow_color'];
    ?>
    <div class="caption">
     <p class="title" style="font-size:<?php echo esc_html($font_size); ?>px; text-shadow:<?php echo esc_html($shadow1); ?>px <?php echo esc_html($shadow2); ?>px <?php echo esc_html($shadow3); ?>px #<?php echo esc_html($shadow4); ?>; color:#<?php echo esc_html($font_color); ?>;"><?php echo nl2br($catch); ?></p>
     <p class="title desc" style="font-size:<?php echo esc_html($desc_font_size); ?>px; text-shadow:<?php echo esc_html($shadow1); ?>px <?php echo esc_html($shadow2); ?>px <?php echo esc_html($shadow3); ?>px #<?php echo esc_html($shadow4); ?>; color:#<?php echo esc_html($font_color); ?>;"><?php echo nl2br($desc); ?></p>
     <?php
          if($options['show_video_catch_button'] == 1) {
            $url = $options['video_button_url'];
            $target = $options['video_button_target'];
            $button_text = $options['video_catch_button'];
     ?>
     <a class="button" href="<?php echo esc_url($url); ?>"<?php if($target == 1) { echo ' target="_blank"'; }; ?>><?php echo esc_html($button_text); ?></a>
     <?php }; // END button ?>
    </div><!-- END .caption -->
    <?php }; // END catch ?>
    <img class="image" src="<?php echo esc_attr($image[0]); ?>" alt="" />
    <div class="overlay"></div>
   </div>
  </div><!-- END #header_slider -->
  <?php
           }; // END if has image
         }; // END mobile device
       }; // END header content type
  ?>
  <?php }; // END if is front page ?>

 </div><!-- END #top -->

 <div id="main_contents" class="clearfix">
