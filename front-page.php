<?php
$options = get_desing_plus_option();

// 料金システム表/機能比較表コンテンツ --------------------------------------
// 必要なページビルダーCSSを読み込むようにする
if ( $options['show_index_price_function'] == 1 ) {
    $show_index_price_function = 0;
    for ( $i = 1; $i <= 3; $i++ ) {
        if ( empty( $options['index_price_function_type'.$i] ) ||
             empty( $options['index_price_function_postid'.$i] ) ||
             empty( $options['index_price_function_num'.$i] ) ) {
            continue;
        }

        // 料金システム表
        if ( $options['index_price_function_type'.$i] == 'type2' &&
             function_exists( 'page_builder_widget_price_table_sctipts_styles' ) &&
             function_exists( 'the_page_builder_price_table' ) ) {
            page_builder_widget_price_table_sctipts_styles(true);
            $show_index_price_function++;

        // 料金システム表/機能比較表コンテンツ
        } elseif ( $options['index_price_function_type'.$i] == 'type3' &&
                   function_exists( 'page_builder_widget_functional_comparison_sctipts_styles' ) &&
                   function_exists( 'the_page_builder_functional_comparison' ) ) {
            page_builder_widget_functional_comparison_sctipts_styles(true);
            $show_index_price_function++;
        }
    }
}

get_header();
?>

<?php
     // Three box content -----------------------------------------------------------------
     if( $options['show_three_box'] == 1 ) {
?>
<div id="index_3box">
 <?php
      $headline = $options['three_box_headline'];
      $headline_font_size = $options['three_box_headline_font_size'];
      $headline_desc = $options['three_box_headline_desc'];
      $headline_desc_font_size = $options['three_box_headline_desc_font_size'];
      if($headline || $headline_desc) {
 ?>
 <div id="index_3box_header" class="animation_element">
  <?php if($headline) { ?>
  <h3 class="headline rich_font" style="font-size:<?php echo esc_html($headline_font_size); ?>px;"><?php echo esc_html($headline); ?></h3>
  <?php }; ?>
  <?php if($headline_desc) { ?>
  <div class="desc" style="font-size:<?php echo esc_html($headline_desc_font_size); ?>px;">
   <?php echo wpautop(esc_html($headline_desc)); ?>
  </div>
  <?php }; ?>
 </div>
 <?php }; ?>
 <div id="index_3box_list" class="animation_element clearfix">
  <?php
       for($i=1; $i<= 3; $i++):
         $icon = $options['three_box_icon'.$i];
         $color = $options['three_box_color'.$i];
         $catch = $options['three_box_catch'.$i];
         $desc = $options['three_box_desc'.$i];
         $url = $options['three_box_url'.$i];
         $target = $options['three_box_target'.$i];
  ?>
  <div class="box clearfix" style="background:#<?php echo esc_html($color); ?>;">
   <?php if(!empty($icon)) { ?>
   <p class="icon three_box_<?php echo esc_html($icon); ?> num<?php echo $i; ?>"><span><?php echo esc_html($icon); ?></span></p>
   <?php }; ?>
   <?php if(!empty($catch)) { ?>
   <h4 class="catch"><?php echo nl2br(esc_html($catch)); ?></h4>
   <?php }; ?>
   <?php if(!empty($desc)) { ?>
   <p class="desc"><?php echo nl2br(esc_html($desc)); ?></p>
   <?php }; ?>
   <?php if(!empty($url)) { ?>
   <div class="link"><a href="<?php echo esc_url($url); ?>"<?php if ( $target ) { echo ' target="_blank"'; } ?>><?php echo __('Read more', 'tcd-w'); ?></a></div>
   <?php }; ?>
  </div><!-- END .box -->
  <?php endfor; ?>
 </div><!-- END #index_3box_list -->
</div><!-- END #index_3box -->
<?php }; ?>


<?php
     // Free space
    if ( $options['index_free_content1'] ) {
      $free_content = apply_filters( 'the_content', $options['index_free_content1'] );
      if ( $free_content ) {
?>
<div id="index_free_content1" class="index_free_content post_content animation_element">
 <?php echo $free_content; ?>
</div>
<?php
      }
    }
?>


<?php
     // Center image -----------------------------------------------------------------
     if( $options['show_index_center_image'] == 1 ) {
       $catch = $options['index_center_image_catch'];
       $font_size = $options['index_center_image_catch_font_size'];
       $desc = $options['index_center_image_desc'];
       $desc_font_size = $options['index_center_image_desc_font_size'];
       $font_color = $options['index_center_image_font_color'];
       $shadow1 = $options['index_center_image_font_shadow_a'];
       $shadow2 = $options['index_center_image_font_shadow_b'];
       $shadow3 = $options['index_center_image_font_shadow_c'];
       $shadow4 = $options['index_center_image_font_shadow_color'];
       $url = $options['index_center_image_url'];
       $target = $options['index_center_image_target'];
       if(is_mobile()){
        if(!empty($options['index_center_image_mobile'])):
         $image = wp_get_attachment_image_src( $options['index_center_image_mobile'], 'full');
        else:
         if(!empty($options['index_center_image'])){
          $image = wp_get_attachment_image_src( $options['index_center_image'], 'full');
         }
        endif;
       }else{
         $image = wp_get_attachment_image_src( $options['index_center_image'], 'full');     
       }
       $use_button = $options['show_index_center_image_button'];
       $button_text = $options['index_center_image_button'];
?>
<div id="index_center_image" class="animation_element">
 <div class="wide_image" style="background:url(<?php echo esc_attr($image[0]); ?>) no-repeat center center; background-size:cover;">
  <div class="caption">
   <h3 class="title rich_font" style="font-size:<?php echo esc_html($font_size); ?>px; text-shadow:<?php echo esc_html($shadow1); ?>px <?php echo esc_html($shadow2); ?>px <?php echo esc_html($shadow3); ?>px #<?php echo esc_html($shadow4); ?>; color:#<?php echo esc_html($font_color); ?>;"><?php echo nl2br(esc_html($catch)); ?></h3>
   <div class="desc" style="font-size:<?php echo esc_attr( $desc_font_size ); ?>px; text-shadow:<?php echo esc_html($shadow1); ?>px <?php echo esc_html($shadow2); ?>px <?php echo esc_html($shadow3); ?>px #<?php echo esc_html($shadow4); ?>; color:#<?php echo esc_html($font_color); ?>;">
    <?php echo wpautop(esc_html($desc)); ?>
   </div>
   <?php if($use_button == 1) { ?>
   <a class="button" href="<?php echo esc_url($url); ?>"<?php if($target == 1) { echo ' target="_blank"'; }; ?>><?php echo esc_html($button_text); ?></a>
   <?php }; ?>
  </div>
 </div>
</div><!-- END #index_center_image -->
<?php }; ?>


<?php
     // Free space
    if ( $options['index_free_content2'] ) {
      $free_content = apply_filters( 'the_content', $options['index_free_content2'] );
      if ( $free_content ) {
?>
<div id="index_free_content2" class="index_free_content post_content animation_element">
 <?php echo $free_content; ?>
</div>
<?php
      }
    }
?>


<?php
     // Four box content -----------------------------------------------------------------
     if( $options['show_four_box'] == 1 ) {
?>
<div id="index_4box" class="animation_element">
 <div id="index_4box_list" class="clearfix">
  <?php
       for($i=1; $i<= 4; $i++):
         $catch = $options['four_box_catch'.$i];
         $desc = $options['four_box_desc'.$i];
         $url = $options['four_box_url'.$i];
         $image = wp_get_attachment_image_src( $options['four_box_image'.$i], 'size1');
  ?>
  <div class="box num<?php echo $i; ?> clearfix">
   <?php if(!empty($url)) { ?><a class="link" href="<?php echo esc_url($url); ?>"><?php } else { ?><div class="link"><?php } ?>
    <?php if(!empty($image)) { ?>
    <div class="image"><img src="<?php echo esc_attr($image[0]); ?>" alt="" /></div>
    <?php }; ?>
    <?php if(!empty($catch)) { ?>
    <h4 class="catch"><?php echo nl2br(esc_html($catch)); ?></h4>
    <?php }; ?>
    <?php if(!empty($desc)) { ?>
    <p class="desc"><?php echo nl2br(esc_html($desc)); ?></p>
    <?php }; ?>
   <?php if(!empty($url)) { ?></a><?php } else { ?></div><?php } ?>
  </div><!-- END .box -->
  <?php endfor; ?>
 </div><!-- END #index_4box_list -->
</div><!-- END #index_4box -->
<?php }; ?>


<?php
     // Free space
    if ( $options['index_free_content3'] ) {
      $free_content = apply_filters( 'the_content', $options['index_free_content3'] );
      if ( $free_content ) {
?>
<div id="index_free_content3" class="index_free_content post_content animation_element">
 <?php echo $free_content; ?>
</div>
<?php
      }
    }
?>

<?php
     // News list --------------------------------------
     if( $options['show_index_news_list'] == 1 ) {
?>
<div id="index_news_list" class="animation_element">
 <?php
      $headline = $options['index_news_headline'];
      $button = $options['index_news_button'];
      $post_num = $options['index_news_list_num'];
      $post_archive_link = get_post_type_archive_link('news');
 ?>
 <div class="index_list_header clearfix">
  <h3 class="headline rich_font"><?php echo esc_html($headline); ?></h3>
  <a class="index_archive_link" href="<?php echo esc_url($post_archive_link); ?>"><?php echo esc_html($button); ?></a>
 </div>
 <ol class="news_list clearfix">
  <?php
       $args = array('post_type' => 'news', 'posts_per_page' => $post_num, 'ignore_sticky_posts' => 1, 'orderby' => 'date', 'order' => 'DESC');

       $news_list = new WP_Query($args);

       if ($news_list->have_posts()) { while ($news_list->have_posts()) : $news_list->the_post();

         $category_data = get_the_terms($post->ID , 'news-cat');
         if(!empty($category_data)) {
           foreach( (array) $category_data as $cat_data ) {
             $cat_id = $cat_data->term_id;
             $cat_name = $cat_data->name;
             $cat_slug = $cat_data->slug;
           };
           $cat_meta_data = get_option("taxonomy_$cat_id");
           $cat_color =  $cat_meta_data['news-cat_color'];
           if(empty($cat_color)) {
             $cat_color = $options['pickedcolor1'];
           };
           $cat_link = get_term_link($cat_slug, 'news-cat');
        };
  ?>
  <li class="clearfix">
   <?php if ( $options['index_news_show_date'] ){ ?><p class="date"><time class="entry-date updated" datetime="<?php the_modified_time('c'); ?>"><?php the_time('Y.m.d'); ?></time></p><?php }; ?>
   <?php if ( $options['index_news_show_category'] ){ if(!empty($category_data)){ ?><div class="category"><a style="background:#<?php echo esc_html($cat_color); ?>;" href="<?php echo esc_url($cat_link); ?>"><?php echo esc_html($cat_name); ?></a></div><?php }; }; ?>
   <p class="title"><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php trim_title(50); ?></a></p>
  </li>
  <?php endwhile; wp_reset_query(); }; ?>
 </ol><!-- END .work_list -->
 <div class="mobile_archive_link">
  <a class="index_archive_link" href="<?php echo esc_url($post_archive_link); ?>"><?php echo esc_html($button); ?></a>
 </div>
</div><!-- END #index_news_list -->
<?php }; ?>


<?php
     // Free space
    if ( $options['index_free_content4'] ) {
      $free_content = apply_filters( 'the_content', $options['index_free_content4'] );
      if ( $free_content ) {
?>
<div id="index_free_content4" class="index_free_content post_content animation_element">
 <?php echo $free_content; ?>
</div>
<?php
      }
    }
?>


<?php
     // Work list --------------------------------------
     if( $options['show_index_work_list'] == 1 ) {
?>
<div id="index_work_list" class="animation_element">
 <?php
      $headline = $options['index_work_headline'];
      $button = $options['index_work_button'];
      $post_num = $options['index_work_list_num'];
      $post_archive_link = get_post_type_archive_link('work');
 ?>
 <div class="index_list_header clearfix">
  <h3 class="headline rich_font"><?php echo esc_html($headline); ?></h3>
  <a class="index_archive_link" href="<?php echo esc_url($post_archive_link); ?>"><?php echo esc_html($button); ?></a>
 </div>
 <div id="work_list_wrap">
  <ol class="work_list clearfix">
   <?php
        $args = array('post_type' => 'work', 'posts_per_page' => $post_num, 'ignore_sticky_posts' => 1, 'orderby' => 'date', 'order' => 'DESC');

        $work_list = new WP_Query($args);

        if ($work_list->have_posts()) { while ($work_list->have_posts()) : $work_list->the_post();

          $work_catch = get_post_meta($post->ID, 'work_catch', true);
          $work_catch = mb_substr($work_catch, 0, 42 ,"utf-8");
          $count_work_catch = mb_strlen($work_catch,"utf-8");
          if($count_work_catch < 42) {
            $work_catch = $work_catch;
          } else {
            $work_catch = $work_catch . '...';
          };
          $category_data = get_the_terms($post->ID , 'work-cat');

          if(!empty($category_data)) {
            foreach( (array) $category_data as $cat_data ) {
              $cat_id = $cat_data->term_id;
              $cat_name = $cat_data->name;
              $cat_slug = $cat_data->slug;
            };
            $cat_meta_data = get_option("taxonomy_$cat_id");
            $cat_color =  $cat_meta_data['work-cat_color'];
            if(empty($cat_color)) {
              $cat_color = $options['pickedcolor1'];
            };
            $cat_link = get_term_link($cat_slug, 'work-cat');
          };
   ?>

   <li class="box">
    <h4 class="title"><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php trim_title(20); ?></a></h4>
    <a class="image" href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php if(has_post_thumbnail()) { the_post_thumbnail('size4'); } else { ?><img src="<?php echo bloginfo('template_url'); ?>/img/common/no_image1.gif" title="" alt="" /><?php }; ?></a>
    <?php if ( $options['index_work_show_category'] ){ if(!empty($category_data)) { ?><div class="category"><a style="background:#<?php echo esc_html($cat_color); ?>;" href="<?php echo esc_url($cat_link); ?>"><?php echo esc_html($cat_name); ?></a></div><?php }; }; ?>
    <?php if(!empty($work_catch)) { ?><p class="catch"><?php echo $work_catch; ?></p><?php }; ?>
    <?php if ( $options['index_work_show_date'] ){ ?><p class="date"><time class="entry-date updated" datetime="<?php the_modified_time('c'); ?>"><?php the_time('Y.m.d'); ?></time></p><?php }; ?>
   </li>
   <?php endwhile; wp_reset_query(); }; ?>
  </ol><!-- END .work_list -->
  <div class="mobile_archive_link">
   <a class="index_archive_link" href="<?php echo esc_url($post_archive_link); ?>"><?php echo esc_html($button); ?></a>
  </div>
 </div><!-- END #work_list_wrap -->
</div><!-- END #index_work_list -->
<?php }; ?>


<?php
     // Free space
    if ( $options['index_free_content5'] ) {
      $free_content = apply_filters( 'the_content', $options['index_free_content5'] );
      if ( $free_content ) {
?>
<div id="index_free_content5" class="index_free_content post_content animation_element">
 <?php echo $free_content; ?>
</div>
<?php
      }
    }
?>


<?php
     // Blog list --------------------------------------
     if( $options['show_index_blog_list'] == 1 ) {
?>
<div id="index_blog_list" class="animation_element">
 <?php
      $headline = $options['index_blog_headline'];
      $button = $options['index_blog_button'];
      $post_num = $options['index_blog_list_num'];
      $post_archive_link = get_permalink( get_option('page_for_posts') );
 ?>
 <div class="index_list_header clearfix">
  <h3 class="headline rich_font"><?php echo esc_html($headline); ?></h3>
  <a class="index_archive_link" href="<?php echo esc_url($post_archive_link); ?>"><?php echo esc_html($button); ?></a>
 </div>
 <ol class="blog_list">
  <?php
       $args = array('post_type' => 'post', 'posts_per_page' => $post_num, 'ignore_sticky_posts' => 1, 'orderby' => 'date', 'order' => 'DESC');

       $blog_list = new WP_Query($args);

       if ($blog_list->have_posts()) { while ($blog_list->have_posts()) : $blog_list->the_post();

         $category_data = get_the_terms($post->ID , 'category');

         foreach( (array) $category_data as $cat_data ) {
           $cat_id = $cat_data->term_id;
           $cat_name = $cat_data->name;
           $cat_slug = $cat_data->slug;
         };

         $cat_meta_data = get_option("cat_$cat_id");

         $cat_color =  $cat_meta_data['category_color'];
         if(empty($cat_color)) {
           $cat_color = $options['pickedcolor1'];
         };
         $cat_link = get_term_link($cat_slug, 'category');
  ?>
  <li class="box">
   <a class="image" href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php if(has_post_thumbnail()) { if($options['index_blog_use_retina'] == 1) { the_post_thumbnail('size5'); } else { the_post_thumbnail('size3'); }; } else { ?><img src="<?php echo bloginfo('template_url'); ?>/img/common/no_image3.gif" title="" alt="" /><?php }; ?></a>
   <ul class="meta clearfix">
    <?php if ( $options['index_blog_show_category'] ){ ?><li class="category"><a style="background:#<?php echo esc_html($cat_color); ?>;" href="<?php echo esc_url($cat_link); ?>"><?php echo esc_html($cat_name); ?></a></li><?php }; ?>
    <?php if ( $options['index_blog_show_date'] ){ ?><li class="date"><time class="entry-date updated" datetime="<?php the_modified_time('c'); ?>"><?php the_time('Y.m.d'); ?></time></li><?php }; ?>
   </ul>
   <p class="title"><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php trim_title(50); ?></a></p>
  </li>
  <?php endwhile; wp_reset_query(); }; ?>
 </ol><!-- END .blog_list -->
 <div class="mobile_archive_link">
  <a class="index_archive_link" href="<?php echo esc_url($post_archive_link); ?>"><?php echo esc_html($button); ?></a>
 </div>
</div><!-- END #index_blog_list -->
<?php }; ?>


<?php
     // Free space
    if ( $options['index_free_content6'] ) {
      $free_content = apply_filters( 'the_content', $options['index_free_content6'] );
      if ( $free_content ) {
?>
<div id="index_free_content6" class="index_free_content post_content animation_element">
 <?php echo $free_content; ?>
</div>
<?php
      }
    }
?>


<?php
    // 料金システム表/機能比較表コンテンツ --------------------------------------
    if ( ! empty( $show_index_price_function ) ) :
?>
<div id="index_price_function" class="post_content animation_element">
<?php
        for ( $i = 1; $i <= 3; $i++ ) {
            if ( empty( $options['index_price_function_type'.$i]) ||
                 empty( $options['index_price_function_postid'.$i] ) ||
                 empty( $options['index_price_function_num'.$i] ) ) {
                continue;
            }

            // 料金システム表
            if ( $options['index_price_function_type'.$i] == 'type2' &&
                 function_exists( 'the_page_builder_price_table' ) ) {
                echo ' <div class="index_price_function index_price_function-type2" class="animation_element">'."\n";
                the_page_builder_price_table($options['index_price_function_postid'.$i], $options['index_price_function_num'.$i]);
                echo "\n </div>\n";

            // 料金システム表/機能比較表コンテンツ
            } elseif ( $options['index_price_function_type'.$i] == 'type3' &&
                       function_exists( 'the_page_builder_functional_comparison' ) ) {
                echo ' <div class="index_price_function index_price_function-type3" class="animation_element">'."\n";
                the_page_builder_functional_comparison($options['index_price_function_postid'.$i], $options['index_price_function_num'.$i]);
                echo "\n </div>\n";
            }
        }
?>
</div>
<?php
    endif;
?>


<?php
     // Free space
    if ( $options['index_free_content7'] ) {
      $free_content = apply_filters( 'the_content', $options['index_free_content7'] );
      if ( $free_content ) {
?>
<div id="index_free_content7" class="index_free_content post_content animation_element">
 <?php echo $free_content; ?>
</div>
<?php
      }
    }
?>


<?php get_footer(); ?>
